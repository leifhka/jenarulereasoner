package org.leifhka.jenarulereasoner;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.stream.Collectors;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import picocli.CommandLine;
import picocli.CommandLine.ParameterException;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.rulesys.BuiltinRegistry;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.reasoner.rulesys.RuleReasoner;

@Command(
    name = "JenaRuleReasoner",
    descriptionHeading = "%n@|bold DESCRIPTION:|@%n",
    parameterListHeading = "%n@|bold PARAMETERS:|@%n",
    optionListHeading = "%n@|bold OPTIONS:|@%n",
    footerHeading = "%n",
    description = "Simple wrapper around Apache Jena's RuleReasoner for applying rules to models.",
    footer = "@|bold EXAMPLES:|@%n"
    //mixinStandardHelpOptions = true
)
public class JenaRuleReasoner {

    @Option(names = {"--help"}, usageHelp = true,
        description = {"Display this help message."})
    public boolean usageHelpRequested;

    @Option(names = {"-m", "--mode"},
        description = {"The mode to execute, either expand or query."})
    public String mode = "expand";

    @Option(names = {"-o", "--out"},
        description = {"The path to write result to."})
    public String outPath;

    @Option(names = {"-q", "--query"},
        description = {"The path to the file containing a query to execute over the inferred model."})
    public String queryPath;

    @Parameters(description = {"The RDF-files to use."})
    public List<String> modelPaths;

    @Option(names = {"-r", "--rules"},
        description = {"The rules to use for inference. If no rules are provided, the input models will be used directly as normal non-inferencing models."})
    public String rulesPath;

    public static void main(String[] args) {

        JenaRuleReasoner reasoner = new JenaRuleReasoner();
        CommandLine cli = new CommandLine(reasoner);
        try {
            cli.parse(args);
        } catch (ParameterException ex) {
            System.err.println(ex.getMessage());
            return;
        }

        if (cli.isUsageHelpRequested()) {
            cli.usage(System.out);
        } else {
            reasoner.execute();
        }
    }

    public void execute() {

        checkArgs();
        Model model = makeModel();
        OutputStream out = makeOutput();

        if (this.mode.equals("expand")) {
            model.write(out, "TTL");
        } else if (this.mode.equals("query")) {
            executeQuery(model, out);
        } else {
            error("Unknown mode " + this.mode + ". See --help for usage.");
        }
    }

    private void checkArgs() {

        if (this.modelPaths.isEmpty()) {
            error("No RDF-files provided, aborting. See --help for usage.");
        }

        if (this.mode.equals("query") && this.queryPath == null) {
            error("Mode set to query, but no query-file provided, aborting. See --help for usage.");
        }
    }

    private Model makeModel() {

        Model model = ModelFactory.createDefaultModel();
        for (String modelPath : this.modelPaths) {
            model.read(modelPath);
        }

        if (this.rulesPath != null) {
            List<Rule> rules = Rule.rulesFromURL(this.rulesPath);
            RuleReasoner reasoner = new GenericRuleReasoner(rules);
            return ModelFactory.createInfModel(reasoner, model);
        } else {
            return model;
        }
    }

    private OutputStream makeOutput() {

        if (this.outPath == null) {
            return System.out;
        } else {
            try {
                return new FileOutputStream(this.outPath);
            } catch (IOException ex) {
                error("Error when opening file " + this.outPath + " for writing: " + ex.getMessage());
            }
            return null;
        }
    }

    private void executeQuery(Model model, OutputStream out) {

        try {
            Path path = Paths.get(this.queryPath);
            String queryString = Files.lines(path).collect(Collectors.joining("\n"));
            Query query = QueryFactory.create(queryString) ;

            QueryExecution qexec = QueryExecutionFactory.create(query, model);

            ResultSet results = qexec.execSelect() ;
            ResultSetFormatter.out(out, results, query) ;
        } catch (IOException ex) {
            error("Error when executing query: " + ex.getMessage());
        }
    }

    private void error(String msg) {
        System.err.println("ERROR: " + msg);
        System.exit(1);
    }
}
